/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transformations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 18:02:43 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/11 17:07:37 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_matrix	*scale_matrix(double x, double y, double z)
{
	t_matrix	*scale;

	if (!(scale = ft_matnew(4, 4)))
		return (NULL);
	(scale->m)[0][0] = x;
	(scale->m)[1][1] = y;
	(scale->m)[2][2] = z;
	(scale->m)[3][3] = 1;
	return (scale);
}

t_matrix	*transla_matrix(double x, double y, double z)
{
	t_matrix	*transla;

	if (!(transla = ft_matnew(4, 4)))
		return (NULL);
	(transla->m)[0][0] = 1;
	(transla->m)[1][1] = 1;
	(transla->m)[2][2] = 1;
	(transla->m)[3][3] = 1;
	(transla->m)[0][3] = x;
	(transla->m)[1][3] = y;
	(transla->m)[2][3] = z;
	return (transla);
}

t_matrix	*xrot_matrix(double x)
{
	t_matrix	*xrot;

	if (!(xrot = ft_matnew(4, 4)))
		return (NULL);
	(xrot->m)[0][0] = 1;
	(xrot->m)[1][1] = cos(x);
	(xrot->m)[1][2] = -sin(x);
	(xrot->m)[2][1] = sin(x);
	(xrot->m)[2][2] = cos(x);
	(xrot->m)[3][3] = 1;
	return (xrot);
}

t_matrix	*yrot_matrix(double y)
{
	t_matrix	*yrot;

	if (!(yrot = ft_matnew(4, 4)))
		return (NULL);
	(yrot->m)[0][0] = cos(y);
	(yrot->m)[0][2] = sin(y);
	(yrot->m)[1][1] = 1;
	(yrot->m)[2][0] = -sin(y);
	(yrot->m)[2][2] = cos(y);
	(yrot->m)[3][3] = 1;
	return (yrot);
}

t_matrix	*zrot_matrix(double z)
{
	t_matrix	*zrot;

	if (!(zrot = ft_matnew(4, 4)))
		return (NULL);
	(zrot->m)[0][0] = cos(z);
	(zrot->m)[0][1] = -sin(z);
	(zrot->m)[1][0] = sin(z);
	(zrot->m)[1][1] = cos(z);
	(zrot->m)[2][2] = 1;
	(zrot->m)[3][3] = 1;
	return (zrot);
}
