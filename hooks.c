/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 19:32:22 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 15:16:01 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	display_commands(t_env *env)
{
	mlx_string_put(env->mlx, env->win, 10, 10, 0x00FFFFFF, "Arrows : rotation");
	mlx_string_put(env->mlx, env->win, 10, 30, 0x00FFFFFF,
						"W,A,S,D,Q,E : translation");
	mlx_string_put(env->mlx, env->win, 10, 50, 0x00FFFFFF,
						"Numpad(+)/numpad(-) : zoom");
	mlx_string_put(env->mlx, env->win, 10, 70, 0x00FFFFFF,
						"Numpad(9)/numpad(6) : Z scale");
	mlx_string_put(env->mlx, env->win, 10, 90, 0x00FFFFFF, "Numpad(0) : reset");
	mlx_string_put(env->mlx, env->win, 10, 110, 0x00FFFFFF,
						"1 : Low height colors");
	mlx_string_put(env->mlx, env->win, 10, 130, 0x00FFFFFF,
						"2 : Land scale colors");
}

int		expose_hook(t_env *env)
{
	int		i;
	int		j;

	mlx_clear_window(env->mlx, env->win);
	env->img = mlx_new_image(env->mlx, env->winx, env->winy);
	env->imgdata = init_imgdata(env->img, env);
	i = 0;
	while (i < (env->data)->nbline)
	{
		j = 0;
		while (j < (env->data)->nbcol)
		{
			draw_line(env, env->data, i, j);
			j++;
		}
		i++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	mlx_destroy_image(env->mlx, env->img);
	display_commands(env);
	z_buffer_clr(env);
	return (0);
}

int		key_hook(int keycode, t_env *env)
{
	if (keycode == KBD_ESC)
		end_fdf(env);
	else if (keycode == KBD_W || keycode == KBD_S || keycode == KBD_A
				|| keycode == KBD_D || keycode == KBD_Q || keycode == KBD_E)
		transla_control(keycode, env);
	else if (keycode == KBD_UP || keycode == KBD_DOWN
				|| keycode == KBD_LEFT || keycode == KBD_RIGHT)
		rotation_control(keycode, env);
	else if (keycode == PAD_PLUS || keycode == PAD_MINUS || keycode == PAD_9
				|| keycode == PAD_6)
		scale_control(keycode, env);
	else if (keycode == PAD_0)
		other_control(keycode, env);
	else if (keycode == KBD_1)
		env->color_set = low_height_color_set;
	else if (keycode == KBD_2)
		env->color_set = land_scale_color_set;
	expose_hook(env);
	return (0);
}

void	multi(t_matrix *a, t_data *data, double *p, int *c)
{
	int		i;

	i = -1;
	while (++i < 3)
	{
		p[i] = (a->m)[i][0] * c[1] + (a->m)[i][1] * c[0]
			+ (a->m)[i][2] * (data->tab)[c[0]][c[1]] + (a->m)[i][3];
	}
}

void	transform_data(t_env *env)
{
	int		c[2];
	double	p[3];

	if (!(env->transfo))
		return ;
	c[0] = 0;
	while (c[0] < (env->data)->nbline)
	{
		c[1] = 0;
		while (c[1] < (env->data)->nbcol)
		{
			multi(env->transfo, env->data, p, c);
			p[0] += (env->winx) / 2;
			p[2] += (env->winy) / 2;
			((env->data)->conv)[c[0]][c[1]][0] = p[0];
			((env->data)->conv)[c[0]][c[1]][1] = p[1];
			((env->data)->conv)[c[0]][c[1]][2] = p[2];
			(c[1])++;
		}
		(c[0])++;
	}
}
