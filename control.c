/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   control.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 17:06:39 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/19 14:47:38 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			transla_control(int keycode, t_env *env)
{
	t_matrix	*mod;
	t_matrix	*tmp;

	mod = NULL;
	tmp = env->obj;
	if (keycode == KBD_A)
		mod = transla_matrix(-XY_TRANS, 0, 0);
	else if (keycode == KBD_D)
		mod = transla_matrix(XY_TRANS, 0, 0);
	else if (keycode == KBD_Q)
		mod = transla_matrix(0, 0, -Z_TRANS);
	else if (keycode == KBD_E)
		mod = transla_matrix(0, 0, Z_TRANS);
	else if (keycode == KBD_S)
		mod = transla_matrix(0, XY_TRANS, 0);
	else if (keycode == KBD_W)
		mod = transla_matrix(0, -XY_TRANS, 0);
	if (mod == NULL)
		return ;
	ft_matdel(&(env->transfo));
	if (!(env->obj = ft_matmultidel(mod, tmp)))
		return ;
	env->transfo = ft_matmulti(env->icam, env->obj);
	transform_data(env);
}

static t_matrix	*xy_rotation(double rot, t_env *env)
{
	t_matrix	*zrot;
	t_matrix	*xyrot;
	t_matrix	*tmp;

	zrot = zrot_matrix(-(env->cam_zrot));
	xyrot = xrot_matrix(rot);
	if (zrot == NULL || xyrot == NULL)
		return (NULL);
	tmp = ft_matmultidel(xyrot, zrot);
	zrot = zrot_matrix(env->cam_zrot);
	if (tmp == NULL || zrot == NULL)
		return (NULL);
	xyrot = ft_matmultidel(zrot, tmp);
	return (xyrot);
}

void			rotation_control(int keycode, t_env *env)
{
	t_matrix	*mod;
	t_matrix	*tmp;

	mod = NULL;
	tmp = env->cam;
	if (keycode == KBD_LEFT)
		mod = zrot_matrix(Z_ROT);
	else if (keycode == KBD_RIGHT)
		mod = zrot_matrix(-Z_ROT);
	else if (keycode == KBD_UP)
		mod = xy_rotation(-XY_ROT, env);
	else if (keycode == KBD_DOWN)
		mod = xy_rotation(XY_ROT, env);
	if (mod == NULL)
		return ;
	if (keycode == KBD_LEFT || keycode == KBD_RIGHT)
		env->cam_zrot += (-2 * (keycode == KBD_RIGHT) + 1) * Z_ROT;
	ft_matdel(&(env->transfo));
	ft_matdel(&(env->icam));
	if (!(env->cam = ft_matmultidel(mod, tmp))
		|| !(env->icam = ft_mattransp(env->cam)))
		return ;
	env->transfo = ft_matmulti(env->icam, env->obj);
	transform_data(env);
	expose_hook(env);
}

void			scale_control(int keycode, t_env *env)
{
	t_matrix	*mod;
	t_matrix	*tmp;

	mod = NULL;
	tmp = env->obj;
	if (ZOOM == 0)
		return ;
	if (keycode == PAD_PLUS)
		mod = scale_matrix(ZOOM, ZOOM, ZOOM);
	else if (keycode == PAD_MINUS)
		mod = scale_matrix(1 / ZOOM, 1 / ZOOM, 1 / ZOOM);
	else if (keycode == PAD_6)
		mod = scale_matrix(1, 1, ZOOM);
	else if (keycode == PAD_9)
		mod = scale_matrix(1, 1, 1 / ZOOM);
	if (mod == NULL)
		return ;
	ft_matdel(&(env->transfo));
	if (!(env->obj = ft_matmultidel(mod, tmp)))
		return ;
	env->transfo = ft_matmulti(env->icam, env->obj);
	transform_data(env);
}

void			other_control(int keycode, t_env *env)
{
	t_matrix	*obj;
	t_matrix	*cam;

	obj = env->obj;
	cam = env->cam;
	if (keycode == PAD_0)
	{
		ft_matdel(&(env->icam));
		ft_matdel(&(env->transfo));
		if (reset_transfo(env) >= 0)
		{
			ft_matdel(&obj);
			ft_matdel(&cam);
		}
		else
		{
			env->obj = obj;
			env->cam = cam;
			env->icam = ft_matinvert(cam);
		}
	}
	transform_data(env);
}
