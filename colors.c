/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/12 16:26:05 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/13 15:45:07 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

unsigned int	low_height_color_set(double z, t_env *env)
{
	unsigned int	color;

	color = 0x00FFFFFF;
	if (z >= 1)
		color = 0x00FF0000;
	if (z >= 2)
		color = 0x0000FF00;
	if (z >= 3)
		color = 0x000000FF;
	if (z >= 10)
		color = 0x00FFFF00;
	if (z <= -3)
		color = 0x0000FFFF;
	return (mlx_get_color_value(env->mlx, color));
}

void			shade(unsigned int col1, unsigned int col2,
						double percent, unsigned int *color)
{
	unsigned int	sub1;
	unsigned int	sub2;
	unsigned int	diff;
	unsigned int	offset;
	unsigned int	mask;

	if (col1 == col2)
		return ;
	offset = 0;
	mask = 0xFF;
	while (offset < 32)
	{
		sub2 = col2 & mask;
		sub1 = col1 & mask;
		if (sub2 < sub1)
			diff = ((sub1 - sub2) >> offset) * percent;
		else
			diff = ((sub2 - sub1) >> offset) * percent;
		*color += (sub2 < sub1 ? -1 : 1) * diff * (mask / 0xFF);
		mask *= 0x100;
		offset += 8;
	}
}

unsigned int	land_scale_color_set(double z, t_env *env)
{
	static unsigned int	colors[8] = {0x000034, 0x00007f, 0x0000FF, 0x00FFFF,
										0xFFFF00, 0x00FF00, 0x00FF00, 0xFFFFFF};
	static double		heights[8] = {-3000, -700, -100, -10, 0, 50, 800, 1200};
	unsigned int		color;
	int					i;

	if (z < heights[0])
		color = colors[0];
	else if (z >= heights[7])
		color = colors[7];
	else
	{
		i = 1;
		while (z > heights[i])
			i++;
		color = colors[i - 1];
		shade(colors[i - 1], colors[i],
				(z - heights[i - 1]) / (heights[i] - heights[i - 1]), &color);
	}
	return (mlx_get_color_value(env->mlx, color));
}
