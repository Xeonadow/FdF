/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 18:37:25 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 12:15:13 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int		check_data(char *line)
{
	int		point;

	point = 0;
	if (*line == '+' || *line == '-')
		(line)++;
	while (ft_isspace(*line) == 0 && *line != '\0')
	{
		if (*line == '.' && point == 0)
			point = 1;
		else if (ft_isdigit(*line) == 0)
			return (-1);
		(line)++;
	}
	return (0);
}

static double	*atodsplit(char *line, int *nbcol)
{
	double	*tabline;
	int		nbd;
	int		i;

	nbd = ft_nbword(line);
	if ((tabline = (double *)malloc(sizeof(double) * nbd)) == NULL)
		return (NULL);
	i = 0;
	while (i < nbd)
	{
		while (ft_isspace(*line) == 1 && *line != '\0')
			line++;
		if (check_data(line) < -1)
			return (NULL);
		tabline[i] = -ft_atod(line);
		while (ft_isspace(*line) == 0 && *line != '\0')
			line++;
		i++;
	}
	if (*nbcol < 0)
		*nbcol = nbd;
	else if (nbd != *nbcol)
		return (NULL);
	return (tabline);
}

static int		sub_parsing(char *line, t_data *d)
{
	double	**tab_tmp;
	int		j;

	tab_tmp = d->tab;
	if (!(d->tab = (double **)malloc(sizeof(double *) * d->nbline + 1)))
		return (-1);
	j = -1;
	while (++j < d->nbline)
		d->tab[j] = tab_tmp[j];
	free(tab_tmp);
	if (((d->tab)[d->nbline] = atodsplit(line, &(d->nbcol))) == NULL)
		return (-1);
	free(line);
	(d->nbline)++;
	return (0);
}

int				parsing(int fd, t_data *d)
{
	char	*line;
	int		ret;

	d->nbline = 0;
	d->nbcol = -1;
	if ((d->tab = (double **)malloc(sizeof(double *) * 1)) == NULL)
		return (-1);
	while ((ret = ft_gnl(fd, &line)) > 0)
	{
		if (sub_parsing(line, d) < 0)
			return (-1);
	}
	if (ret < 0)
		return (-1);
	if (*line != '\0')
		return (sub_parsing(line, d));
	return (0);
}
