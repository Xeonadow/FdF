/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 16:08:27 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 15:21:57 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	bresenham(int *p0, int *p1, int *coord, t_env *env)
{
	int		diff[2];
	int		sign[2];
	int		x;
	int		y;
	int		e;

	diff[0] = ft_abs(p1[0] - p0[0]);
	diff[1] = ft_abs(p1[2] - p0[2]);
	if (diff[1] < 0)
		diff[1] = INT_MAX;
	sign[0] = p1[0] > p0[0] ? 1 : -1;
	sign[1] = p1[2] > p0[2] ? 1 : -1;
	e = (diff[0] > diff[1] ? diff[0] : -diff[1]) / 2;
	x = 0;
	y = 0;
	while ((p0[0] + x != p1[0] || p0[2] + y != p1[2]) && p0[0] + x >= 0
			&& p0[0] + x < env->winx && p0[2] + y >= 0 && p0[2] + y < env->winy)
	{
		pix_put(coord, x, y, env);
		x += (e > -diff[0] ? sign[0] : 0);
		y += (e < diff[1] ? sign[1] : 0);
		e = e - (e > -diff[0] ? diff[1] : 0) + (e < diff[1] ? diff[0] : 0);
	}
	pix_put(coord, x, y, env);
}

void	horizontal(t_env *env, t_data *data, int i, int j)
{
	int		coord[4];
	int		*p0;

	coord[0] = i;
	coord[1] = j;
	coord[2] = i;
	coord[3] = j + 1;
	p0 = (data->conv)[i][j];
	if (p0[0] < 0 || p0[0] >= env->winx || p0[2] < 0 || p0[2] >= env->winy)
	{
		ft_swap(coord + 1, coord + 3);
		bresenham((data->conv)[i][j + 1], (data->conv)[i][j], coord, env);
	}
	else
		bresenham((data->conv)[i][j], (data->conv)[i][j + 1], coord, env);
}

void	vertical(t_env *env, t_data *data, int i, int j)
{
	int		coord[4];
	int		*p0;

	coord[0] = i;
	coord[1] = j;
	coord[2] = i + 1;
	coord[3] = j;
	p0 = (data->conv)[i][j];
	if (p0[0] < 0 || p0[0] >= env->winx || p0[2] < 0 || p0[2] >= env->winy)
	{
		ft_swap(coord + 0, coord + 2);
		bresenham((data->conv)[i + 1][j], (data->conv)[i][j], coord, env);
	}
	else
		bresenham((data->conv)[i][j], (data->conv)[i + 1][j], coord, env);
}

void	draw_line(t_env *env, t_data *data, int i, int j)
{
	if (j + 1 < data->nbcol)
		horizontal(env, data, i, j);
	if (i + 1 < data->nbline)
		vertical(env, data, i, j);
}
