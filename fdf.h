/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:12:58 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 14:27:53 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <math.h>
# include <float.h>
# include <limits.h>
# include "mlx.h"
# include "libft.h"
# include "keys.h"

# define DEF_XYSCALE 30
# define DEF_ZSCALE DEF_XYSCALE
# define DEF_WIN 50
# define XY_ROT 0.05 * M_PI
# define Z_ROT 0.05 * M_PI
# define XY_TRANS 10
# define Z_TRANS 10
# define ZOOM 1.2

typedef struct	s_imgdata
{
	char		*img;
	int			bppixel;
	int			size_line;
	int			endian;
	int			x;
	int			y;
}				t_imgdata;

typedef struct	s_data
{
	double		**tab;
	int			***conv;
	int			nbline;
	int			nbcol;
}				t_data;

typedef struct	s_env
{
	t_data			*data;
	void			*mlx;
	void			*win;
	int				winx;
	int				winy;
	void			*img;
	t_imgdata		*imgdata;
	double			**z_buffer;
	t_matrix		*obj;
	t_matrix		*cam;
	t_matrix		*icam;
	t_matrix		*transfo;
	double			cam_zrot;
	unsigned int	(*color_set)(double, struct s_env *);
}				t_env;

int				parsing(int fd, t_data *d);
t_matrix		*default_cam(void);
t_matrix		*default_obj(t_data *data);
double			**init_z_buffer(int winx, int winy);
int				***init_conv(int nbline, int nbcol);
t_imgdata		*init_imgdata(void *img, t_env *env);
int				reset_transfo(t_env *env);
void			end_fdf(t_env *env);

t_matrix		*scale_matrix(double x, double y, double z);
t_matrix		*transla_matrix(double x, double y, double z);
t_matrix		*xrot_matrix(double x);
t_matrix		*yrot_matrix(double y);
t_matrix		*zrot_matrix(double z);

void			transform_data(t_env *env);
void			pix_put(int *co, int x, int y, t_env *env);
void			draw_line(t_env *env, t_data *data, int i, int j);
void			z_buffer_clr(t_env *env);

unsigned int	low_height_color_set(double z, t_env *env);
unsigned int	land_scale_color_set(double z, t_env *env);

void			transla_control(int keycode, t_env *env);
void			rotation_control(int keycode, t_env *env);
void			scale_control(int keycode, t_env *env);
void			other_control(int keycode, t_env *env);

int				expose_hook(t_env *env);
int				key_hook(int keycode, t_env *env);

#endif
