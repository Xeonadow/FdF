/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel_to_image.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/02 15:58:46 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/19 18:54:15 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

unsigned int	get_color(int *coord, int x, int y, t_env *env)
{
	double	height1;
	double	height2;
	int		*conv1;
	int		*conv2;
	int		dist;

	height1 = -((env->data)->tab)[coord[0]][coord[1]];
	height2 = -((env->data)->tab)[coord[2]][coord[3]];
	conv1 = ((env->data)->conv)[coord[0]][coord[1]];
	conv2 = ((env->data)->conv)[coord[2]][coord[3]];
	dist = ft_max(ft_abs(conv1[0] - conv2[0]), ft_abs(conv1[2] - conv2[2]));
	x = ft_abs(x);
	y = ft_abs(y);
	return ((env->color_set)(height1 + (height2 - height1) *
						(fmax(x, y) / (double)dist), env));
}

void			put_pix_img(t_imgdata *img, int x, int y, unsigned int color)
{
	int		i;
	int		j;
	int		bytespp;

	if (x < 0 || x >= img->x || y < 0 || y >= img->y)
		return ;
	bytespp = img->bppixel / 8 + (img->bppixel % 8 != 0);
	i = y * img->size_line + x * bytespp;
	j = 0;
	while (j < bytespp)
	{
		if (img->endian == 1)
			(img->img)[i + (bytespp - j - 1)] = (color >> (j * 8)) & 0xff;
		else
			(img->img)[i + j] = (color >> (j * 8)) & 0xff;
		j++;
	}
}

void			pix_put(int *co, int x, int y, t_env *env)
{
	double			**z_buf;
	int				p0[3];

	z_buf = env->z_buffer;
	p0[0] = ((env->data)->conv)[co[0]][co[1]][0] + x;
	p0[1] = ((env->data)->conv)[co[0]][co[1]][1];
	p0[2] = ((env->data)->conv)[co[0]][co[1]][2] + y;
	if (p0[0] < 0 || p0[0] >= env->winx || p0[2] < 0 || p0[2] >= env->winy ||
						p0[1] < z_buf[p0[2]][p0[0]])
		return ;
	put_pix_img(env->imgdata, p0[0], p0[2], get_color(co, x, y, env));
	z_buf[p0[2]][p0[0]] = p0[1];
}
