/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:12:07 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 11:49:00 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		z_buffer_clr(t_env *env)
{
	int		x;
	int		y;

	y = 0;
	while (y < env->winy)
	{
		x = 0;
		while (x < env->winx)
		{
			(env->z_buffer)[y][x] = -DBL_MAX;
			x++;
		}
		y++;
	}
}

int			reset_transfo(t_env *env)
{
	env->obj = default_obj(env->data);
	env->cam = default_cam();
	if (!(env->obj) || !(env->cam))
		return (-1);
	env->icam = ft_mattransp(env->cam);
	if (!(env->icam))
		return (-1);
	env->transfo = ft_matmulti(env->icam, env->obj);
	if (!(env->transfo))
		return (-1);
	env->cam_zrot = -M_PI / 4;
	return (0);
}

void		end_fdf(t_env *env)
{
	ft_matdel(&(env->transfo));
	ft_matdel(&(env->icam));
	ft_matdel(&(env->cam));
	ft_matdel(&(env->obj));
	while ((env->data)->nbline > 0)
	{
		((env->data)->nbline)--;
		free(((env->data)->tab)[(env->data)->nbline]);
	}
	free((env->data)->tab);
	mlx_destroy_window(env->mlx, env->win);
	exit(0);
}

int			fdf(t_data *data)
{
	t_env		env;

	env.data = data;
	env.winx = data->nbcol * DEF_WIN > 2500 ? 2500 : data->nbcol * DEF_WIN;
	env.winy = data->nbline * DEF_WIN > 1300 ? 1300 : data->nbline * DEF_WIN;
	if (!(env.mlx = mlx_init()) || env.winx <= 0 || env.winy <= 0
		|| !(env.win = mlx_new_window(env.mlx, env.winx, env.winy, "fdf"))
		|| !(env.z_buffer = init_z_buffer(env.winx, env.winy))
		|| !((env.data)->conv = init_conv(data->nbline, data->nbcol))
		|| reset_transfo(&env) < 0)
		return (-1);
	env.color_set = low_height_color_set;
	z_buffer_clr(&env);
	transform_data(&env);
	mlx_expose_hook(env.win, expose_hook, &env);
	mlx_hook(env.win, 2, 1, key_hook, &env);
	mlx_loop(env.mlx);
	return (0);
}

int			main(int argc, char **argv)
{
	int		fd;
	t_data	data;

	if (argc < 2)
		ft_putstr("error : No file specified\n");
	else if (argc > 2)
		ft_putstr("error : Too many arguments\n");
	else
	{
		fd = open(argv[1], O_RDONLY);
		if (fd < 0)
			perror("error ");
		else
		{
			if (parsing(fd, &data) < 0 || data.nbcol <= 0 || data.nbline <= 0)
				ft_putstr("error : Invalid file\n");
			else if (fdf(&data) < 0)
				perror("error ");
			close(fd);
		}
	}
	return (0);
}
