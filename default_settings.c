/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   default_settings.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 16:19:51 by lpousse           #+#    #+#             */
/*   Updated: 2016/05/20 14:26:10 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_matrix	*default_cam(void)
{
	t_matrix	*cam;
	t_matrix	*cam_tmp;
	t_matrix	*transfo;

	cam = transla_matrix(0, 50, 0);
	transfo = xrot_matrix(-atan(M_SQRT1_2));
	if (cam == NULL || transfo == NULL)
		return (NULL);
	cam_tmp = ft_matmultidel(transfo, cam);
	transfo = zrot_matrix(-M_PI / 4);
	if (cam_tmp == NULL || transfo == NULL)
		return (NULL);
	cam = ft_matmulti(transfo, cam_tmp);
	return (cam);
}

t_matrix	*default_obj(t_data *data)
{
	t_matrix	*obj;
	t_matrix	*obj_tmp;
	t_matrix	*transfo;

	obj_tmp = scale_matrix(DEF_XYSCALE, DEF_XYSCALE, DEF_ZSCALE);
	transfo = transla_matrix(-DEF_XYSCALE * (data->nbcol / 2),
								-DEF_XYSCALE * (data->nbline / 2), 0);
	if (!transfo || !obj_tmp)
		return (NULL);
	obj = ft_matmultidel(transfo, obj_tmp);
	return (obj);
}

double		**init_z_buffer(int winx, int winy)
{
	double	**z_buffer;
	int		y;

	if (!(z_buffer = (double **)malloc(sizeof(double *) * winy)))
		return (NULL);
	y = 0;
	while (y < winy)
	{
		if (!(z_buffer[y] = (double *)malloc(sizeof(double) * winx)))
			return (NULL);
		y++;
	}
	return (z_buffer);
}

int			***init_conv(int nbline, int nbcol)
{
	int		***conv;
	int		x;
	int		y;

	if (!(conv = (int ***)malloc(sizeof(int **) * nbline)))
		return (NULL);
	y = 0;
	while (y < nbline)
	{
		if (!(conv[y] = (int **)malloc(sizeof(int *) * nbcol)))
			return (0);
		x = 0;
		while (x < nbcol)
		{
			if (!(conv[y][x] = (int *)malloc(sizeof(int) * 3)))
				return (NULL);
			x++;
		}
		y++;
	}
	return (conv);
}

t_imgdata	*init_imgdata(void *img, t_env *env)
{
	t_imgdata	*imgdata;

	imgdata = (t_imgdata *)malloc(sizeof(t_imgdata));
	imgdata->img = mlx_get_data_addr(img, &(imgdata->bppixel),
								&(imgdata->size_line), &(imgdata->endian));
	imgdata->x = env->winx;
	imgdata->y = env->winy;
	return (imgdata);
}
