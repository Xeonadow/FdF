/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 18:41:18 by lpousse           #+#    #+#             */
/*   Updated: 2016/04/29 18:42:40 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

# define KBD_ESC 53
# define KBD_TAB 48
# define KBD_1 18
# define KBD_2 19
# define KBD_3 20
# define KBD_4 21
# define KBD_5 23
# define KBD_6 22
# define KBD_SPACE 49
# define KBD_UP 126
# define KBD_DOWN 125
# define KBD_LEFT 123
# define KBD_RIGHT 124
# define KBD_W 13
# define KBD_S 1
# define KBD_A 0
# define KBD_D 2
# define KBD_Q 12
# define KBD_E 14
# define PAD_PLUS 69
# define PAD_MINUS 78
# define PAD_0 82
# define PAD_1 83
# define PAD_2 84
# define PAD_3 85
# define PAD_4 86
# define PAD_5 87
# define PAD_6 88
# define PAD_7 89
# define PAD_8 91
# define PAD_9 92

#endif
